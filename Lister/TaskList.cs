﻿/*
 * Name: Lister
 * Created By: Glenn Mitchell
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Lister
{
    public class TaskList
    {
        // list to hold all the list items
        private readonly Dictionary<string, TaskItem> _list;

        // unique name for this list
        public string ListName { get; private set; }
        
        // construct an empty task list giving it a name
        public TaskList(string name)
        {
            ListName = name;
            _list = new Dictionary<string, TaskItem>();
        }

        public void AddTaskItem(string title)
        {
            var newItem = new TaskItem()
            {
                Title = title,
                Description = "",
                Priority = "Normal",
                DateCompleted = new DateTime(),
                DueDate = new DateTime()
            };
        }

        public void RemoveTaskItem(string itemTitle)
        {
            _list.Remove(itemTitle);
        }

        public void SetTitle(string originalItemTitle, string newItemTitle)
        {
            // check the item exists in the list
            if (!ItemExists(originalItemTitle)) return;

            // get a reference to the item
            var originalItem = _list[originalItemTitle];

            // make sure it exists
            if (originalItem != null)
            {
                // create a new item with the new title but the original item info
                var newItem = new TaskItem()
                {
                    Title = newItemTitle,
                    Description = originalItem.Description,
                    Priority = originalItem.Priority,
                    DateCompleted = originalItem.DateCompleted,
                    DueDate = originalItem.DueDate
                };

                // remove the original item
                _list.Remove(originalItemTitle);

                // add the new item
                _list.Add(newItem.Title, newItem);

            }
        }

        public void SetDescription(string itemTitle, string description)
        {
            // check the item exists in the list
            if (!ItemExists(itemTitle)) return;

            _list[itemTitle].Description = description;
        }

        public void SetDueDate(string itemTitle, DateTime dueDateTime)
        {
            // check the item exists in the list
            if (!ItemExists(itemTitle)) return;

            _list[itemTitle].DueDate = dueDateTime;
        }

        public void SetCompleteDate(string itemTitle, DateTime completeDateTime)
        {
            // check the item exists in the list
            if (!ItemExists(itemTitle)) return;

            _list[itemTitle].DateCompleted = completeDateTime;
        }

        public void SetPriority(string itemTitle, string priority)
        {
            // check the item exists in the list
            if (!ItemExists(itemTitle)) return;

            _list[itemTitle].Priority = priority;
        }

        // get an array of task items
        public string[] GetTaskItems()
        {
            // create the array to be returned
            var itemArray = new string[_list.Count];

            var currentIndex = 0;

            // loop through and add items to the array
            foreach (var item in _list)
            {
                itemArray[currentIndex] = item.Value.ToString();
                currentIndex++;
            }

            return itemArray;
        }

        public bool ItemExists(string taskTitle)
        {
            var item = _list[taskTitle];
            return item != null;
        }

        public void Reset()
        {
            _list.Clear();
        }
    }
}