﻿/*
 * Name: Lister
 * Created By: Glenn Mitchell
 */

using System;
using System.Text;

namespace Lister
{
    internal class TaskItem
    {
        private const string DELIMETER = "|";

        internal TaskItem()
        {
        }

        internal string Title { get; set; }
        internal string Description { get; set; }
        internal DateTime DueDate { get; set; }
        internal DateTime DateCompleted { get; set; }
        internal string Priority { get; set; }

        // override the ToString()
        internal new string ToString()
        {
            var sb = new StringBuilder();

            sb.Append(Title);
            sb.Append(DELIMETER);

            sb.Append(Description);
            sb.Append(DELIMETER);

            sb.Append(DueDate);
            sb.Append(DELIMETER);

            sb.Append(DateCompleted);
            sb.Append(DELIMETER);

            sb.Append(Priority);

            return sb.ToString();
        }
    }
}