﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lister
{
    public class TaskListManager
    {
        public enum TaskItemPriority
        {
            High,
            Normal,
            Low
        };

        private readonly Dictionary<string, TaskList> _lists;

        public TaskListManager()
        {
            _lists = new Dictionary<string, TaskList>();
        }

        // create a new list with specified name and add it to the list dict
        public void CreateNewList(string listName)
        {
            var newList = new TaskList(listName);
            _lists.Add(listName, newList);
        }

        // remove list with specified name
        public void RemoveList(string listName)
        {
            if (!ListExists(listName)) return;
            _lists.Remove(listName);
        }

        // get a string array containing the list names
        public string[] GetTaskLists()
        {

            // we are creating a copy of the array to ensure the internal
            // data is not being modified.

            var results = new String[_lists.Count];
            _lists.Keys.CopyTo(results, 0);
            return results;
        }

        // return the specified task list
        public string[] GetTaskItemsByListName(string listName)
        {
            if (!ListExists(listName)) return new string[]{};
            return _lists[listName].GetTaskItems();
        }

        // add a new task item to the specified list
        // return true on success, false on failure
        public bool AddItemToList(string listName, string listItemTitle)
        {
            if (!ListExists(listName)) return false;
            _lists[listName].AddTaskItem(listItemTitle);
            return true;
        }

        // remove a list item from specified list
        // return true on success, false on failure
        public bool RemoveItemFromList(string listName, string listItemTitle)
        {
            if (!ListExists(listName)) return false;
            _lists[listName].RemoveTaskItem(listItemTitle);
            return true;
        }

        // clear the specified list of all items (reset)
        public void ClearList(string listName)
        {
            _lists[listName].Reset();
        }

        // method to test that a specified list exists
        // return true if list exists
        private bool ListExists(string listName)
        {
            return _lists[listName] != null;
        }

        // given listname and old item title, update with the new title
        // return true on success, false on failure
        public bool SetTitle(string listName, string originalTitle, string newTitle)
        {
            if (!ListExists(listName)) return false;
            _lists[listName].SetTitle(originalTitle, newTitle);
            return true;
        }

        public bool SetDueDate(string listName, string itemTitle, DateTime dueDate)
        {
            if (!ListExists(listName)) return false;
            _lists[listName].SetDueDate(itemTitle, dueDate);
            return true;
        }
        public bool SetCompleteDate(string listName, string itemTitle, DateTime completeDate)
        {
            if (!ListExists(listName)) return false;
            _lists[listName].SetCompleteDate(itemTitle, completeDate);
            return true;
        }
        public bool SetPriority(string listName, string itemTitle, TaskItemPriority priority)
        {
            if (!ListExists(listName)) return false;
            _lists[listName].SetPriority(itemTitle, priority.ToString());
            return true; ;
        }
        public bool SetDescription(string listName, string itemTitle, string description)
        {
            if (!ListExists(listName)) return false;
            _lists[listName].SetDescription(itemTitle, description);
            return true;
        }
    }
}